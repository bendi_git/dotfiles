#!/bin/sh


cat /sys/class/power_supply/BAT1/status /sys/class/power_supply/BAT1/charge_full /sys/class/power_supply/BAT1/charge_now | tr '\n' ' ' | sed -e "s/Discharging//" -e "s/Charging//" -e "s/Full//" |  awk --field-separator=" " '{printf $1}{printf " "} {printf "%.5s",(($3/$2)*100)} END{printf "%"}'
