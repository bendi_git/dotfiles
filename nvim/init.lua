require('plugins')

require("mason").setup()



local lspconfig = require('lspconfig')

-- Automatically start coq
vim.g.coq_settings = { auto_start = 'shut-up' }

-- Enable some language servers with the additional completion capabilities offered by coq_nvim
local servers = { 'pyright', 'sumneko_lua'}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup(require('coq').lsp_ensure_capabilities({
    -- on_attach = my_custom_on_attach,
  }))
end

---vim.keymap.set("n", "<C-t>", ":NERDTreeToggle, <CR>", {noremap = true}) 


--vim.cmd("autocmd VimEnter * NERDTree")
--vim.cmd("autocmd VimEnter * NERDTreeToggle")

vim.cmd('set noshowmode')
vim.cmd('set termguicolors')
vim.cmd('set nocompatible')
vim.cmd('set showmatch')
vim.cmd('set ignorecase')
vim.cmd('set hlsearch')
vim.cmd('set incsearch')
vim.cmd('set tabstop=4')
vim.cmd('set softtabstop=4')
vim.cmd('set expandtab')
vim.cmd('set shiftwidth=4')
vim.cmd('set autoindent')
vim.cmd('set number')
vim.cmd('set wildmode=longest,list')
vim.cmd('filetype plugin indent on')
vim.cmd('syntax on')
vim.cmd('set mouse=a')
vim.cmd('set clipboard=unnamedplus')
vim.cmd('filetype plugin on')
vim.cmd('set cursorline')
vim.cmd('set ttyfast')
vim.cmd("autocmd vimenter * ++nested colorscheme gruvbox")
